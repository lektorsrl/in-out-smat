<?php

if($_SERVER['PHP_AUTH_USER'] == 'easy' and $_SERVER['PHP_AUTH_PW'] == 'easySmat2021'){
//credenziali OK
}else{
    exit('Credenziali non valide');
}
require_once('config.php');

$segnL = array('E','H','K','O','U','W','B','F','R','S','V','M','Z','D','J','CR','FG','T','C','AU');
$segnN = array('A','E','H','I','L','N','O','P','Q','Y','R','S','V','J','C19','C19DI');
$flag = array('A','B','N','I','G');

$letture = json_decode(file_get_contents('php://input'), true);
Apri_DB('sede');
$cont = 0;
foreach($letture as $lettura){

    //check lettura
    if(strlen($lettura['lettura']) > 0){
        if (is_numeric($lettura['lettura']) == false ){
            exit('La lettura deve essere numerica per id'.$lettura['id']);
        }
    }

    //check date
    if(preg_match('/(0[1-9]|1[0-9]|3[01])\/(0[1-9]|1[012])\/(2[0-9][0-9][0-9]|1[6-9][0-9][0-9])/', $lettura['data_lettura'] ) !== 1 ){
        exit('Formato data non valido per id '.$lettura['id']);
    }

    //check time
    if(preg_match('/^\d+:\d{2}:\d{2}$/', $lettura['ora_lettura'] ) !== 1 ){
        exit('Formato ora non valido per id '.$lettura['id']);
    }

    //check segn1
    if(strlen($lettura['lettura']) > 0){
        if(strlen($lettura['segn1']) > 0 ){
            if(in_array($lettura['segn1'], $segnL) === false){
                exit('Formato segn1 non valido per id '.$lettura['id']);
            }
        }
    }
    if(strlen($lettura['lettura']) == 0){
        if(in_array($lettura['segn1'], $segnN) === false){
            exit('Formato segn1 non valido per id '.$lettura['id']);
        }
    }
    if(!is_numeric($lettura['id'])){
        exit('id non valido '.$lettura['id']);
    }
    if(strlen($lettura['flag']) > 0){
        if(in_array($lettura['flag'], $segnN) === false){
            exit('Flag non valido per id '.$lettura['id']);
        }
    }
    if(strlen($lettura['lettura']) > 0 ){
        if(strlen(trim($lettura['consumo'])) == 0){
            exit('consumo non valido per id '.$lettura['id']);
        }  
    }
    $query = "select * from letture where id = " .$lettura['id']." and stato = 'LET'";
    $result = mysql_query($query);
    if($result){
        $row = mysql_fetch_assoc($result);
        $query_insert = "insert into log_aggiornamenti_letture_smat (lettura_id,lettura,data_lettura,ora_lettura,nota,segn1,flag, consumo) values (".$row['id'].",'".$row['lettura']."','".$row['data_lettura']."','".$row['ora_lettura']."','".$row['nota']."','".$row['segn1']."','".$row['flag']."','".$row['consumo']."')";                        
        $result_insert = mysql_query($query_insert);
        if(!$result_insert){
            exit("Problema nella registrazione del log per l'id .".$lettura['id'].". Procedura interrotta. ");
        }
        if($lettura['flag'] == ""){
            $flag = "L";
        }else{
            $flag = $lettura['flag'];
        }
        $query_update = "update letture set editata_da_programma=2, lettura='".$lettura['lettura']."'
                    ,data_lettura='".$lettura['data_lettura']."'
                    ,ora_lettura='".$lettura['ora_lettura']."'
                    ,nota='".$lettura['note']."'
                    ,segn1='".$lettura['segn1']."'
                    ,flag='".$flag."'
                    ,consumo='".$lettura['consumo']."' where id=".$lettura['id']." and stato = 'LET'";
        $result_update = mysql_query($query_update);
        if(!$result_update){
            exit("Problema di update per l'id ".$lettura['id']. ". Procedura interrotta");
        }else{
            $cont++;
        }               
    }
}
echo "Aggiornati ".$cont. " record.";
?>